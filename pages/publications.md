# **Colombian Sign Language Dataset (CoL-SLTD)**
---
## Publications
1. Sign language translation using motion filters and attention models (Poster) - KHIPU 2019 - **Status:** _Presented_ - (Version 1.0) - [link poster](https://www.researchgate.net/publication/339205487_Sign_language_translation_using_motion_filters_and_attention_models?channel=doi&linkId=5e43ed7ea6fdccd9659c186e&showFulltext=true)   
2. How important is motion in sign language translation? - IET Computer Vision - **Status:** _Published_ - (Version 1.0) - [link paper](https://ietresearch.onlinelibrary.wiley.com/doi/full/10.1049/cvi2.12037)  
3. Understanding Motion in Sign Language: A new Structured Translation Dataset - Asian Conference on Computer Vision 2020 - **Status:** _Published_ - (Version 2.0) - [link paper](https://openaccess.thecvf.com/content/ACCV2020/html/Rodriguez_Understanding_Motion_in_Sign_Language_A_New_Structured_Translation_Dataset_ACCV_2020_paper.html) 
