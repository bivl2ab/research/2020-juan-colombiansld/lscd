# **Colombian Sign Language Dataset (CoL-SLTD)**
---
## History Versions

* 2020-04-08 : Version 1.0 : First available version, you can download the 510 videos (mp4) following this [link] _Retired_
* 2020-11-30 : Version 2.0 : Second available version, you can download the 1020 videos (mp4) following this [link](https://drive.google.com/drive/folders/1k40Qfpzdc0TsVYIHOCK13Kj1_Iyjcoi3?usp=sharing)  
* 2020-11-30 : Version 2.0 update: Temporal annotations for the first repetition (Excel) and splits data, you can download them following this [link](https://drive.google.com/drive/folders/1ubMpHY3BoBOHHeZSo80Ts_r37yuKmhe-?usp=sharing)  
* Upcoming : PNG and optical flow frames    
* Upcoming : Dense and Open Pose information  
* 2021-06-01 : Full CoLSLTD, Flownet 2, OpenPose, DensePose, Brox Optical flow and RGB  [link decompressed files (700GB aprox)](https://drive.google.com/drive/folders/1ppmJL7XvvOAsDk8i4EofjtWViHN_3KRt?usp=sharing)  
* 2021-06-01 : Full CoLSLTD, Flownet 2, OpenPose, DensePose, Brox Optical flow and RGB  [link zip file](https://drive.google.com/file/d/1sW9fE7w35rEyF0n_JEdOn8WQmJ5c8gom/view?usp=sharing)   
