# **Colombian Sign Language Dataset (CoL-SLTD)**
---
## Introduction

![CoL-SLTD_cover](https://gitlab.com/bivl2ab/research/2020-juan-colombiansld/lscd/-/raw/master/images/CoL-SLTD_dataset.png)

**CoL-SLTD** is a Colombian sign language dataset built mainly for the evaluation of algorithms that translate video sign language expressions into their corresponding written translation (Spanish). This dataset was created in the **Biomedical Imaging, vision and learning laboratory group (BivL2ab)**, located in the Universidad Industrial de Santander - UIS (Bucaramanga - Colombia). The dataset focuses on capturing well-formed (Subject-Verb-Object) and short utterances with structural kinematic dependencies. Also, the dataset contains different types of sentences such as affirmative, negative and interrogative expresions. 

The other pages will describe all the information corresponding to the recording process, pre-processing, annotations, updates and publications associated with the dataset.

_This dataset has been approved by the ethics committee of the educational institution (UIS). This approval include an informed consent and participants authorize the use of this information for research community._

![logos](https://gitlab.com/bivl2ab/research/2020-juan-colombiansld/lscd/-/raw/master/images/logos.png)