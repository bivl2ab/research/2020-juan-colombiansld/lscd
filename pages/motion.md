# **Colombian Sign Language Dataset (CoL-SLTD)**
---

## The Sign Motion Component

![example1](https://gitlab.com/bivl2ab/research/2020-juan-colombiansld/lscd/-/raw/master/images/CoL_SLTD_sign_example.png)

To support the analysis of the motion component, a kinematic vector field descriptor was calculated for each video sign. For this purpose, an optical flow approach with the capability to recover large displacements and relative sharp motions was selected to capture motion signs descriptions at low or high temporal resolutions (_Brox, T., & Malik, J. (2010). Large displacement optical flow: descriptor matching in variational motion estimation_). Such cases are almost present in any sign, which reports different velocity and acceleration profiles but are especially observed in the exclamation marks. The resultant velocity field for a particular frame is obtained from a variational Euler-Lagrange minimization, that include local and non-local restrictions between two consecutive frames. To capture large displacements, a non-local assumption is introduced by matching key-points with similar velocity field patterns. The captured flow field volume result highly described, keeping spatial coherence and aggregating motion information patterns as a low-level representation. 

![example2](https://gitlab.com/bivl2ab/research/2020-juan-colombiansld/lscd/-/raw/master/images/SLT_ColombianExamples.png)