# **Colombian Sign Language Translation Dataset (CoL-SLTD)**
---
## Annotations

This dataset has been annotated with translations into written language (Colombian Spanish) and the first repetition of sentences has been annotated with glosses under the supervision of a professional sign interpreter. Additionally, it is included which videos are part of each proposed split. To download, click on the following [link](https://drive.google.com/drive/folders/1MFT8JB27ylDsT-zexPgx-Q9NofLbDzx5?usp=sharing). 
